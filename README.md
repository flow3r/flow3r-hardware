# flow3r

This repo contains the design files of the Chaos Communication Camp 2023 badge called flow3r.

![flow3r](pics/flow3r.jpg "flow3r")

The heart of the flow3r is an ESP32-S3 with 8 MiB of PSRAM and 16 MiB of flash memory. With this platform, you have access to a plethora of existing software projects. It has a round color display with a resolution of 240x240 pixels, two built-in speakers, 3.5mm audio input and output ports, 40 RGB LEDs along the edges, and a USB-C connector. Inside, you’ll find a microSD card slot. As always, the electrical and mechanical design, as well as our firmware, are open source and can be edited with free software. The battery is optional this time, and its connector is compatible with the MCH2022 Badge battery. Of course, you can also just power the flow3r using something like a computer or power bank.

flow3rs main components are two PCBs called top (pink) and bottom (white) connected by a 3d-printed spacer.

## repo structure

| folder     | content                                           |
|:------------|-------------------------------------------------:|
| boards     | Kicad schematics & pcb layout of top & bottom  |
| datasheets | datasheets of used electrical components          |
| mechanical | Full FreeCAD 3d model of all components of flower |

## Info

Blog post: https://events.ccc.de/2023/06/05/camp23-the-flow3r-badge/

Matrix channel: https://matrix.to/#/#flow3rbadge:events.ccc.de

Mastodon: https://chaos.social/@flow3rbadge

