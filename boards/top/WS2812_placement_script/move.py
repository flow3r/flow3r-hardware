# Coordinates of the LEDs.  Tuple consists of
# (x offset from center in mm, y offset from center in mm, angle in degrees)

import pcbnew
import coordinates

pcb = pcbnew.LoadBoard("top.kicad_pcb")

for i, led in enumerate(coordinates.led_positions):
    c = pcb.FindFootprintByReference("LED%d" % (1 + ((i + 9) % 40)))
    p = pcbnew.VECTOR2I_MM(100 + led[0], 100 - led[1])
    c.SetPosition(p)
    c.SetOrientation(pcbnew.EDA_ANGLE(led[2] + 180, pcbnew.DEGREES_T))

pcb.Save("top.kicad_pcb")

